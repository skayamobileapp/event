$(document).ready(function () {
    $('#list-table').DataTable({
    });

});
function clearSearchForm() {
    $(':input', '#searchForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .prop('checked', false)
        .prop('selected', false);
}