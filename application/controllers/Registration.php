<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Registration extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_login_model');
        error_reporting(0);
    }

    /**
     * Index Page for this controller.
     */
    public function index($id)
    {

         $data['inviteeDetails'] = $inviteeDetails = $this->applicant_login_model->getInviteeDetails($id);

       // print_r($data);exit;

         if($_POST) {

            $data = array('status'=>$_POST['status'],'confirmed_date'=>date('Y-m-d H:i:s'));
                $result = $this->applicant_login_model->editEventInvitation($data,$inviteeDetails->id);

            redirect('/registration/thankyou/'.$id);

         }


         $data['eventdetails'] =  $eventDetails  = $this->applicant_login_model->getEventDetails($inviteeDetails->id_event_title);
        $data['eventtemplate']  = $this->applicant_login_model->getTemplate($inviteeDetails->id_event_title);

        $template = $data['eventtemplate'][0]->template;
        $data['message'] = $this->replacemessagestrings($template,$inviteeDetails,$eventDetails);

            $this->loadViews("registration", $this->global, $data, NULL);
    }


    public function attendence($id) {

        $date = date('Y-m-d');
        $data['inviteeDetails'] = $inviteeDetails = $this->applicant_login_model->getInviteeDetails($id);


            $eventDetails  = $this->applicant_login_model->checkattendence($inviteeDetails->id_event_title);

         $data['eventdetails'] =  $eventDetails  = $this->applicant_login_model->getEventDetails($inviteeDetails->id_event_title);
            
        if(!$eventDetails) {

            $eventarray = array('id_event_invitation'=>$inviteeDetails->id,'attendence_date'=>$date,
                'attendence_status'=>'1','id_event_title'=>$inviteeDetails->id_event_title);
            $this->applicant_login_model->addattendence($eventarray);
        } else {
            $eventarray = array('id_event_invitation'=>$inviteeDetails->id,'attendence_date'=>$date,
                'attendence_status'=>'1','id_event_title'=>$inviteeDetails->id_event_title);
            $this->applicant_login_model->updateattendence($eventarray,$eventDetails->id);
        }



    
            $this->loadViews("welcome", $this->global, $data, NULL);
    }


    public function thankyou($id)
    {
          $this->savepdf($id);

        

         $data['inviteeDetails'] = $inviteeDetails = $this->applicant_login_model->getInviteeDetails($id);


        $eventtemplate  = $this->applicant_login_model->getConfirmationTemplate($inviteeDetails->id_event_title);




         $data['eventdetails'] =  $eventDetails  = $this->applicant_login_model->getEventDetails($inviteeDetails->id_event_title);

     $messageReplacedString = $this->replacemessagestrings($eventtemplate[0]->email_template,$inviteeDetails,$eventDetails);


        $subject = "Thank you for Registering";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <admin@agilidigitaltechnologies.com>' . "\r\n";
        $headers .= 'Cc: askiran123@gmail.com' . "\r\n";
        $mail = mail($inviteeDetails->email,$subject,$messageReplacedString,$headers);


 
                    $apiURL = 'https://api.chat-api.com/instance243272/';
                    $token = 'x9bztflvg1ty5fnf';
                    $phone = $inviteeDetails->phone;

                    $arr = json_encode(array(
                            'phone'=>$phone,
                            'body'=>BASE_PATH.$inviteeDetails->uniqueid.".jpeg",
                            'filename'=>$inviteeDetails->uniqueid.".jpeg"
                        ));

                        $url = 'https://api.chat-api.com/instance243272/sendFile?token=x9bztflvg1ty5fnf';


                        $ch = curl_init();

                                // set url
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);

                        //return the transfer as a string
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $arr);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-type:application/json',
                            'Content-length:'.strlen($arr)
                        ));

                        // $output contains the output string
                        $output = curl_exec($ch);

                        // close curl resource to free up system resources
                        curl_close($ch); 

                                // echo $output;


            



    
            $this->loadViews("thankyou", $this->global, $data, NULL);

    }

     public function replacemessagestrings($message,$invitee,$programme) {

        //Name
        // print_R($message);exit;
// print_r($invitee);
        $rsvpencoded = $invitee->uniqueid;

        $rsvplink = BASE_PATH."register/index/$rsvpencoded";


        $message = str_replace("@salutation",$invitee->salutation,$message);
        $message = str_replace("@studentname",$invitee->full_name,$message);
        $message = str_replace("@email",$invitee->email,$message);
        $message = str_replace("@department",$invitee->department,$message);
        $message = str_replace("@staffid",$invitee->staff_id,$message);
        $message = str_replace("@eventname",$programme->title,$message);
        $message = str_replace("@department",$invitee->department,$message);
        $message = str_replace("@designation",$invitee->designation,$message);
        $message = str_replace("@office",$invitee->office,$message);
        $message = str_replace("@sector",$invitee->sector,$message);
        $message = str_replace("@management",$invitee->management,$message);
        $message = str_replace("@part",$invitee->part,$message);
        $message = str_replace("@unit",$invitee->unit,$message);
        $message = str_replace("@rsvplink",$rsvplink,$message);
        $message = str_replace("@company",$invitee->company,$message);
        $message = str_replace("@inviteescategory",$invitee->invitees_category,$message);
        $message = str_replace("@salutation2",$invitee->salutation_two,$message);
        $message = str_replace("@rsvpdeadline",$programme->rsvp_deadline,$message);
        $message = str_replace("@rsvplink",$rsvplink,$message);

        return $message;
    }


    function savepdf($id){

  $inviteeDetails = $this->applicant_login_model->getInviteeDetails($id);

         $path = $_SERVER['DOCUMENT_ROOT'].'/assets/library/phpqrcode/qrlib.php';
          include $path;    
    



    $tempDir = $_SERVER['DOCUMENT_ROOT'].'/assets/qrcode/';
    
    $codeContents = BASE_PATH.'registration/attendence/'.$id;
    
    // we need to generate filename somehow, 
    // with md5 or with database ID used to obtains $codeContents...
    $fileName = date('Ymdhis').'.png';
    
    $pngAbsoluteFilePath = $tempDir.$fileName;



     if (!file_exists($pngAbsoluteFilePath)) {
        QRcode::png($codeContents,$tempDir.$fileName, QR_ECLEVEL_L, 18);
        //echo 'File generated!';
        //echo '<hr />';
    } else {
        //echo 'File already generated! We can use this cached file to speed up site on common codes!';
        //echo '<hr />';
    }
        chmod($tempDir.$fileName,0777);
        $dest = imagecreatefromjpeg($tempDir.'new.jpg');
        $src = imagecreatefrompng($tempDir.$fileName);

        
// (B) WRITE TEXT
$white = imagecolorallocate($dest, 255, 255, 255);
$txt = $inviteeDetails->full_name;
$category1 = "";
$category2 = "";
$category3 = "";
  
        $catarray = explode('*',$inviteeDetails->invitees_category);

        for($i=0;$i<count($catarray);$i++) {
            if($i==0) {
                  $category1 = $catarray[$i]; 
            } 
             if($i==1) {
                  $category2 = $catarray[$i]; 
            } 
             if($i==2) {
                  $category3 = $catarray[$i]; 
            } 
        }
    




$counttxt = strlen($txt);
$font = $_SERVER['DOCUMENT_ROOT'].'/assets/fonts/DejaVuSansCondensed-Bold.ttf';

$diff = $counttxt-10;
 $mainsize = 1000;
 $diffsize = ($diff*15);
 $customsize = $mainsize - $diffsize;
 imagettftext($dest, 50, 0, $customsize, 700, $white,$font, $txt);


 if($category2=='' && $category3=='' &$category1!='') {

    $counttxt = strlen($category1);
$diffcat1 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat1 = ($diffcat1*15);
 $customisecat1 = $mainsize - $diffsizecat1;
 imagettftext($dest, 50, 0, $customisecat1, 2200, $white,$font, $category1);
}

if($category2!='' && $category3=='' &$category1!='') {

    $counttxt = strlen($category1);
$diffcat1 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat1 = ($diffcat1*15);
 $customisecat1 = $mainsize - $diffsizecat1;

 $counttxt = strlen($category2);
$diffcat2 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat2 = ($diffcat2*15);
 $customisecat2 = $mainsize - $diffsizecat2;
 imagettftext($dest, 50, 0, $customisecat1, 2100, $white,$font, $category1);
 imagettftext($dest, 50, 0, $customisecat2, 2200, $white,$font, $category2);

}

if($category2!='' && $category3!='' &$category1!='') {

        $counttxt = strlen($category1);
$diffcat1 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat1 = ($diffcat1*15);
 $customisecat1 = $mainsize - $diffsizecat1;

 $counttxt = strlen($category2);
$diffcat2 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat2 = ($diffcat2*15);
 $customisecat2 = $mainsize - $diffsizecat2;

 $counttxt = strlen($category3);
$diffcat3 = $counttxt-10;
 $mainsize = 1000;
 $diffsizecat3 = ($diffcat3*15);
 $customisecat3 = $mainsize - $diffsizecat3;

 imagettftext($dest, 50, 0, $customisecat1, 2100, $white,$font, $category1);
 imagettftext($dest, 50, 0, $customisecat2, 2200, $white,$font, $category2);
 imagettftext($dest, 50, 0, $customisecat3, 2300, $white,$font, $category3);

}

///qr code//

        $im = imagecopymerge($dest, $src, 1600, 2570, 0, 0, 740, 720, 100); //have to play with these numbers for it to work for you, etc.






       $rand = rand(0000000000,9999999999);
  imagejpeg($dest,$id.".jpeg");

        imagedestroy($dest);
        imagedestroy($src);

       return "1";


    }


}

?>
