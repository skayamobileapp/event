<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Applicant_login_model extends CI_Model
{
      function getInviteeDetails($id) {
         $this->db->select('*');
        $this->db->from('event_invitation');
        $this->db->where('uniqueid', $id);
        $query = $this->db->get();
        return $query->row();
    }

      function getConfirmationTemplate($id){
         $this->db->select('*');
        $this->db->from('event_confirmation_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    

      function getEventDetails($id)
    {
        $this->db->select('*');
        $this->db->from('event_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }



    function checkattendence($id){
      $this->db->select('*');
        $this->db->from('event_attendence');
        $this->db->where('id_event_invitation', $id);
        $query = $this->db->get();
        return $query->row();
    }

      function editEventInvitation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_invitation', $data);
        return TRUE;
    }




    function getTemplate($id){
         $this->db->select('*');
        $this->db->from('event_setting_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function addattendence($data) {
       $this->db->trans_start();
        $this->db->insert('event_attendence', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

      function updateattendence($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_attendence', $data);
        return TRUE;
    }



}

?>