<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Supervisor_login_model extends CI_Model
{
    
    function loginSupervisor($email, $password)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.email', $email);
        // $this->db->where('stu.applicant_status', 'Approved');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        
        return $user = $query->row();
        
        if(!empty($user))
        {
        // echo "<Pre>";print_r($user->password);exit();
            if(md5($password) == $user->password)
            {
        // echo "<Pre>";print_r($user);exit();
                return $user;
            }else
            {
                return array();
            }
        }
        else
        {
            return array();
        }
    }

    function supervisorLastLoginInfo($id_supervisor)
    {
        $this->db->select('ll.created_dt_tm');
        $this->db->where('ll.id_supervisor', $id_supervisor);
        $this->db->order_by('ll.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('supervisor_last_login as ll');

        return $query->row();
    }


    function checkSupervisorEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('student');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getSupervisorInfoByEmail($email)
    {
        $this->db->select('userId, email, name');
        $this->db->from('student');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    function addSupervisorLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}

?>