<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Organisation Details</h3>
        </div>



        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Organisation Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $partnerUniversity->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $partnerUniversity->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $partnerUniversity->short_name; ?>">
                    </div>
                </div>


            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $partnerUniversity->name_in_malay; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website / Url </label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $partnerUniversity->url; ?>">
                    </div>
                </div>



            </div>









            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($partnerUniversity->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($partnerUniversity->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>  


            </div>



        </div>



        <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $partnerUniversity->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $partnerUniversity->email; ?>">
                    </div>
                </div>


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $partnerUniversity->address1 ?>">
                    </div>
                </div>

            </div>





            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $partnerUniversity->address2 ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($partnerUniversity->id_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" 
                                <?php if($partnerUniversity->id_state==$record->id)
                                {
                                    echo "selected";
                                } ?>
                                >
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>




            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $partnerUniversity->city ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $partnerUniversity->zipcode ?>">
                    </div>
                </div>
            </div>


        </div>





        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>








        </div>

    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function codeConcate()
    {
        var d='_';
        document.getElementById('training_complete_code').value = document.getElementById('code').value + d+document.getElementById('training_code').value;
    }

    

    function validateUniversityData()
    {
        if($('#form_award').valid())
        {
            $('#form_award').submit();
        }
    }



    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/pm/partnerUniversity/getStateByCountryForTraining/"+id, function(data, status)
        {
            $("#view_state").html(data);
        });
    }


    function saveData()
    {
        if($('#form_comitee').valid())
        {

        var tempPR = {};
        tempPR['role'] = $("#role").val();
        tempPR['name'] = $("#com_name").val();
        tempPR['nric'] = $("#com_nric").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id;?>;
            $.ajax(
            {
               url: '/pm/partnerUniversity/addComitee',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function deleteOrganisationConitee(id)
    {
        $.ajax(
            {
               url: '/pm/partnerUniversity/deleteComitee/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                short_name: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_partner_category: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                login_id: {
                    required: true
                },
                password : {
                    required: true
                },
                id_bank : {
                    required: true
                },
                account_number : {
                    required: true
                },
                swift_code: {
                    required: true
                },
                bank_address : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_partner_category: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                login_id: {
                    required: "<p class='error-text'>Login ID Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                account_number: {
                    required: "<p class='error-text'>Account Number Required</p>",
                },
                swift_code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                bank_address: {
                    required: "<p class='error-text'>Bank Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    
</script>
