<style type="text/css">
    .page-nav-links > li + li {
    padding-left: 10px;
}

</style>
    <?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
          <li><a href="/event/eventDetails/edit/<?php echo $eventdetails->id;?>">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/<?php echo $eventdetails->id;?>">Participants</a></li>
            <li><a href="/event/eventDetails/communication/<?php echo $eventdetails->id;?>">Invitation</a></li>

            <li><a href="/event/eventDetails/confirmationsetting/<?php echo $eventdetails->id;?>">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failuresetting/<?php echo $eventdetails->id;?>">Not Attendee Mail Setting</a></li>

            <li><a href="/event/eventDetails/setting/<?php echo $eventdetails->id;?>">RSVP Remainder</a></li>
            <li><a href="/event/eventDetails/eventremainder/<?php echo $eventdetails->id;?>">Event Remainder</a></li>

            <li><a href="/event/eventDetails/rsvp/<?php echo $eventdetails->id;?>">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/<?php echo $eventdetails->id;?>">Seat Allotment</a></li>
            <li class="active"><a href="/event/eventDetails/attendence/<?php echo $eventdetails->id;?>">Registration Email</a></li>
        </ul>
      <form id="form_programme" action="" method="post">
         <div class="form-container">
                  <div class="row">
                     <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                            </tr>
                            <tr>
                                 <td>Name : @studentname</td>
                                 <td>Email : @email</td>
                                 <td>Organisation : @organisation</td>
                                 <td>Department : @department</td>
                                 <td>Staff ID : @staffid</td>
                            </tr>
                             <tr>
                                 <td>Full Name : @fullname</td>
                                 <td>Phone : @phone</td>
                                 <td>Location : @location</td>
                                 <td>Unit : @unit</td>
                                 <td>Department : @department</td>
                            </tr>

                            <tr>
                                 <td>RSVP Link : @rsvplink</td>
                                 <td>Event Name : @eventname</td>
                                 <td>Event Start Date : @eventstartdate</td>
                                 <td>Event End Date : @eventenddate</td>
                                 <td>Salutation : @salutation</td>
                            </tr>
                            
                            
                        </table>
                  </div>
         



                  <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message">Email Subject <span class='error-text'>*</span></label>
                            <input type='text' name='subject' id='subject' class="form-control"  value="<?php echo $eventtemplate[0]->email_subject;?>" />

                            <input type='hidden' name='templateid' id='templateid' class="form-control"  value="<?php echo $eventtemplate[0]->id;?>" />


                        </div>
                    </div>

                </div>

                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message"><b>Email Template </b><span class='error-text'>*</span></label>
                            <input type='hidden' name='templateid' id='templateid' class="form-control"  value="<?php echo $eventtemplate[0]->id;?>" />
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $eventtemplate[0]->template;?></textarea>
                        </div>
                    </div>

                </div>


                   <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message"><b>Whatsapp Template</b> <span class='error-text'>*</span></label>

                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="whatsapp_message" id="whatsapp_message"><?php echo $eventtemplate[0]->whatsapp_message;?></textarea>
                        </div>
                    </div>

                </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
                
                
            </div>
    



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "800px"

}); 


CKEDITOR.replace('whatsapp_message',{

  width: "100%",
  height: "800px"

}); 

</script>
