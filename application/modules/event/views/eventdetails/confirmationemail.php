<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li  class="active"><a href="/event/eventDetails/confirmationsetting/1">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failuresetting/1">Not Attende Mail Setting</a></li>


                        <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Program Learning Objective Details</h4>
               <div class="row custom-table">
                     <div class="col-sm-12 custom-table">
                        <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                            </tr>
                            <tr>
                                 <td>Name : @studentname</td>
                                 <td>Email : @email</td>
                                 <td>Organisation : @organisation</td>
                                 <td>Department : @department</td>
                                 <td>Staff ID : @staffid</td>
                            </tr>
                            <tr>
                                 <td>RSVP Link : @rsvplink</td>
                                 <td>Event Name : @eventname</td>
                                 <td>Event Start Date : @eventstartdate</td>
                                 <td>Event End Date : @eventenddate</td>
                                 <td>Salutation : @salutation</td>
                            </tr>
                            
                            
                        </table>
                     </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message">Email Template <span class='error-text'>*</span></label>
                             <input type='hidden' name='templateid' id='templateid' class="form-control"  value="<?php echo $eventtemplate[0]->id;?>" />
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $eventtemplate[0]->email_template;?></textarea>
                        </div>
                    </div>

                </div>

                  <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message">Whatsapp Template <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message_wa" id="message_wa"><?php echo $eventtemplate[0]->whatsap_template;?></textarea>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                </div>
                
            </div>
 




   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "800px"

}); 

CKEDITOR.replace('message_wa',{

  width: "100%",
  height: "800px"

}); 

</script>
