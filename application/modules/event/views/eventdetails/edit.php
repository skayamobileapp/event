<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li class="active"><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>

            <li><a href="/event/eventDetails/confirmationemail/1">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failureemail/1">Not Attende Mail Setting</a></li>

            <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>

      <form id="form_programme" action="" method="post">
           <div class="form-container">
            <h4 class="form-group-title">Event Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="<?php echo $eventdetails->title;?>">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Status <span class='error-text'>*</span></label>

                        <select class="form-control" name='status' id='status'>
                            <?php for($i=0;$i<count($eventStatusList);$i++) {?> 
                                <option value="<?php $eventStatusList[$i]->id;?>"

                                    <?php if($eventdetails->status==$eventStatusList[$i]->id) { echo "selected=selected";} ?>

                                    ><?php echo $eventStatusList[$i]->name;?></option>
                            <?php } ?> 
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Seat Limit <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="limits" name="limits" value="<?php echo $eventdetails->limits;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $eventdetails->start_date;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="end_date" name="end_date"  value="<?php echo $eventdetails->end_date;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event URL <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="text" name="url" value="<?php echo $eventdetails->url;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $eventdetails->address1;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="remarks" name="remarks"  value="<?php echo $eventdetails->remarks;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_name" name="person_name" value="<?php echo $eventdetails->person_name;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_email" name="person_email" value="<?php echo $eventdetails->person_email;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Phone <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_phone" name="person_phone" value="<?php echo $eventdetails->person_phone;?>">
                    </div>
                </div>


            </div>
        </div>

            <div class="form-container">
            <h4 class="form-group-title">Event Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Invitation Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="invitation_start_date" name="invitation_start_date" value="<?php echo $eventdetails->invitation_start_date;?>">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>RSVP Deadline<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="rsvp_deadline" name="rsvp_deadline"  value="<?php echo $eventdetails->rsvp_deadline;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>RSVP Remainder 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="remainder_one" name="remainder_one" value="<?php echo $eventdetails->remainder_one;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>RSVP Remainder 2 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="remainder_two" name="remainder_two" value="<?php echo $eventdetails->remainder_two;?>">
                    </div>
                </div>


               

   </div>
            </div>


             <div class="form-container">
            <h4 class="form-group-title">Event Document or Links</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Document Link <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="link" name="link" value="<?php echo $eventdetails->link;?>">
                    </div>
                </div>

                 

               


            </div>
               <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>

        </div>
     
      </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                sequence: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Sequence required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
