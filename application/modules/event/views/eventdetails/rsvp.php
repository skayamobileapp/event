<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li ><a href="/event/eventDetails/confirmationsetting/1">Attendee Mail Setting</a></li>
            <li ><a href="/event/eventDetails/failuresetting/1">Not Attende Mail Setting</a></li>


                        <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li   class="active"><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>
    <div class="custom-table">
      <table class="table" id="list-table">
          <thead>
            <tr>
             <th>First NAme </th>
    <th>Full Name </th> 
    <th>LAst NAme</th>      
    <th>Phone </th>
    <th>Email </th>
    <th>NRIC </th>
    <th>Confirmed on </th>
            </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
                <td><?php echo $record->first_name;?></td>
    <td><?php echo $record->full_name;?></td>
    <td><?php echo $record->last_name;?></td>
    <td><?php echo $record->phone;?></td>
    <td><?php echo $record->email;?></td>

    <td><?php echo $record->nric;?></td>
    <td><?php echo date('Y-m-d');?></td>    
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
       </div>



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

     function saveData() {


        var tempPR = {};
        tempPR->id_event_days'] = $("#id_event_days").val();
        tempPR->timings'] = $("#timings").val();
        tempPR->programme'] = $("#programme").val();
        tempPR->remarks'] = $("#remarks").val();

      

        eventId = tempPR->id'] = <?php echo $this->eventId;?>;
            $.ajax(
            {
               url: '/event/eventdetails/programme/id/'+eventId,
                type: 'POST',
               data:
               {
                tempData: tempPR,
                files : fd
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
        
    }

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                sequence: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Sequence required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
