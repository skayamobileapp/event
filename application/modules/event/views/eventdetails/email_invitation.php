<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
        <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li class="active"><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li><a href="/event/eventDetails/confirmationemail/1">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failureemail/1">Not Attende Mail Setting</a></li>


                        <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>


      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Invitation Details</h4>
               <div class="row custom-table">
                     <div class="col-sm-12 custom-table">
                        <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                            </tr>
                            <tr>
                                 <td>Name : @studentname</td>
                                 <td>Email : @email</td>
                                 <td>Organisation : @organisation</td>
                                 <td>Department : @department</td>
                                 <td>Staff ID : @staffid</td>
                            </tr>
                            <tr>
                                 <td>RSVP Link : @rsvplink</td>
                                 <td>Event Name : @eventname</td>
                                 <td>Event Start Date : @eventstartdate</td>
                                 <td>Event End Date : @eventenddate</td>
                                 <td>Salutation : @salutation</td>
                            </tr>
                            
                            
                        </table>
                     </div>
                </div>
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message">Email Subject <span class='error-text'>*</span></label>
                            <input type='text' name='subject' id='subject' class="form-control"  value="<?php echo $eventtemplate[0]->email_subject;?>" />

                            <input type='hidden' name='templateid' id='templateid' class="form-control"  value="<?php echo $eventtemplate[0]->id;?>" />


                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message"><b>Email Template </b><span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $eventtemplate[0]->email_template;?></textarea>
                        </div>
                    </div>

                </div>

                  <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message"><b>Whatsapp Template</b> <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message_wa" id="message_wa"><?php echo $eventtemplate[0]->whatsap_template;?></textarea>
                        </div>
                    </div>

                </div>
                
            </div>
     
     <div class="custom-table">
      <table class="table" id="list-table">         
          <tr>
             <td> Send Communication via 
              <select name="send_as" class="form-control">
                <option value="Email">Email</option>
                <option value="Whatsapp">Whatsapp</option>
                <option value="Email_Whatsapp">Email & Whatsapp</option>

              </select>
            </td>
            <td>Select Invitation for Testing<select name='id_invitation' id='id_invitation' class="form-control">
                <option value="">Select</option>
              <?php for($i=0;$i<count($eventprogramme);$i++) {?>
                 <option value="<?php echo $eventprogramme[$i]->id;?>">
                  <?php echo $eventprogramme[$i]->full_name;?></option>
              <?php } ?> 
            </select>
            <td><br/>
                <button type="submit" class="btn btn-primary btn-lg">Save & Send</button></td>
            </div>
        </div>
          </tr>
        </tbody>
        </table>
       </div>




   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "600px"

}); 

CKEDITOR.replace('message_wa',{

  width: "100%",
  height: "600px"

}); 

</script>
