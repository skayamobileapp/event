<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
           <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li ><a href="/event/eventDetails/confirmationsetting/1">Attendee Mail Setting</a></li>
            <li ><a href="/event/eventDetails/failuresetting/1">Not Attende Mail Setting</a></li>


                        <li  class="active"><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
                  <div class="row">
                     <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                                 <th>Text</th>
                            </tr>
                            <tr>
                                 <td>Name : @studentname</td>
                                 <td>Email : @email</td>
                                 <td>Organisation : @organisation</td>
                                 <td>Department : @department</td>
                                 <td>Staff ID : @staffid</td>
                            </tr>
                             <tr>
                                 <td>Full Name : @fullname</td>
                                 <td>Phone : @phone</td>
                                 <td>Location : @location</td>
                                 <td>Unit : @unit</td>
                                 <td>Department : @department</td>
                            </tr>

                            <tr>
                                 <td>RSVP Link : @rsvplink</td>
                                 <td>Event Name : @eventname</td>
                                 <td>Event Start Date : @eventstartdate</td>
                                 <td>Event End Date : @eventenddate</td>
                                 <td>Salutation : @salutation</td>
                            </tr>
                            
                            
                        </table>
                  </div>
         


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                                                      <input type='hidden' name='templateid' id='templateid' class="form-control"  value="<?php echo $eventtemplate[0]->id;?>" />
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $eventtemplate[0]->template;?></textarea>
                        </div>
                    </div>

                </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
                
                
            </div>
    



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "800px"

}); 


CKEDITOR.replace('message1',{

  width: "100%",
  height: "200px"

}); 

</script>
