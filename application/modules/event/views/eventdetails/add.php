<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Event</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Event Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="status" name="status" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Seat Limit <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="limits" name="limits">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="start_date" name="start_date">
                    </div>
                </div>


  <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="end_date" name="end_date" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event URL <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="text" name="url">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1">
                    </div>
                </div>


  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="remarks" name="remarks" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_name" name="person_name">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_email" name="person_email">
                    </div>
                </div>
    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Phone <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_phone" name="person_phone">
                    </div>
                </div>

               

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                sequence: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Sequence required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
