<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/programme/1">Programme</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>

      <form id="form_programme" action="" method="post" enctype="multipart/form-data">
         <div class="form-container">
            <h4 class="form-group-title">Program  Details</h4>
            <div class="row">
              
                <table class="table" width="100%">
         <tr>
            <th>Date</th>
             <th>Time </th>
              <th>PRogramme</th>
               <th>Remarks</th>

        <th>Action</th>           

        </tr>

        <tr>
            
         <td><select name='id_event_days' id='id_event_days' class="input-txt" style="width: 150px;">

       <option value="1">1</option>
            </select>
          </td>
          <td><input type='text' class="input-txt timepicker"  name='timings' id='timings' autocomplete="off" /></td>

            <td><input type='text' class="input-txt"  name='programme' id='programme' style="width: 150px;"/></td>
            <td><input type='text' class="input-txt"  name='remarks' id='remarks' style="width: 150px;"/></td>
          <td><input type='file' class="input-txt"  name='document' id='document' style="width: 150px;"/></td>

 <td><input type="submit" onclick="saveData()" value="Save"/></td>



        </tr>
       
    
    </table>
            </div>
         </div>
     
      </form>
    <div class="custom-table">
      <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Event Day</th>
              <th>Time</th>
              <th>Programme</th>
              <th>Remarks</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                                  <td><?php echo $record->id_event_days ?></td>

                  <td><?php echo $record->timings ?></td>
                  <td><?php echo $record->programme ?></td>
                  <td><?php echo $record->remarks ?></td>
                  <td class="text-center">
                      <a href="#" title="Edit">Delete</a>
                  </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
       </div>



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>


    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                sequence: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Sequence required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 15,
    minTime: '7',
    maxTime: '23:00',
    startTime: '07:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});



</script>
