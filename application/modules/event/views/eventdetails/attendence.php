<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li><a href="/event/eventDetails/confirmationemail/1">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failureemail/1">Not Attende Mail Setting</a></li>


                        <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li class="active"><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>
    <div class="custom-table">
      <table class="table" id="list-table">
          <thead>
            <tr>
             <th>Invitation Category </th> 
    <th>Group </th>
    <th>Salutation One </th>
    <th>Salutation Two </th>
    <th>Full Name </th>
    <th>Designation </th>
    <th>Department </th>
    <th>Company </th>
    <th>Staff ID</th>
    <th>Phone </th>    
    <th>Email </th>
    <th>RSVP Confirmation</th>
    <th>Event Attendence</th> 

  
             </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
                 <td><?php echo $record->invitees_category;?></td>
    <td><?php echo $record->group_name;?></td>
    <td><?php echo $record->salutation;?></td>
    <td><?php echo $record->salutation_two;?></td>
    <td><?php echo $record->full_name;?></td>
    <td><?php echo $record->designation;?></td>
    <td><?php echo $record->department;?></td>
    <td><?php echo $record->company;?></td>
    <td><?php echo $record->staff;?></td>
    <td><?php echo $record->phone;?></td>
    <td><?php echo $record->email;?></td>

    <td><?php if($record->rsvp_confirmation=='1') {
              echo "Confirmed";
     } else { 
    echo "Not Confirmed";
      } ?> 
    </td>
      <?php for($i=0;$i<count($eventdays);$i++) { ?>
      <td>
        <input type='radio' name='id_event_days[]' >Present <br/><input type='radio' name='id_event_days[]' >Absent </td>
      </td>

    <?php } ?>  
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
       </div>



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

