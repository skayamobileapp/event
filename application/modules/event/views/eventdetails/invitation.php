<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">

   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/event/eventDetails/edit/1">Edit</a></li>
            <li class="active"><a href="/event/eventDetails/invitation/1">Participants</a></li>
            <li><a href="/event/eventDetails/communication/1">Invitation</a></li>
            <li><a href="/event/eventDetails/confirmationsetting/1">Attendee Mail Setting</a></li>
            <li><a href="/event/eventDetails/failureemail/1">Not Attende Mail Setting</a></li>


                        <li><a href="/event/eventDetails/setting/1">RSVP Setting</a></li>

            <li><a href="/event/eventDetails/rsvp/1">RSVP Attendence</a></li>
            <li><a href="/event/eventDetails/seat/1">Seat Allotment</a></li>
            <li><a href="/event/eventDetails/attendence/1">Attendence</a></li>
        </ul>
 <div class="form-container">
     <!--  <form name='membership-form' id='member' method='post' enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Participants</h4>
            <div class="row">
    <table class="table" width="100%">
          <tr>
              <th colspan="3">Invitation Details</th>
          </tr>

          <tr>
         
              <td  width="200em">Upload File<span class="reqMark">*</span></td>
              <td  width="200em"><input type="file" class="input-txt" name="activityfile" id="activityfile" value=""></td>
              
              <td><input type="submit" value="Upload File"/></td>
          </tr>
          <tr>

             <td  colspan="3"> <p style="text-align: right;"><a href="/assets/Event_template.csv">Click here to Download Template</a></p></td>

          </tr>
        
         
      
      </table>
    </div>
  </div>
</form> -->
<div class="row">
   <div class="col-sm-12">
    <div class="custom-table">
      <table class="table" id="example">
          <thead>
            <tr>
    <th>Invitation Category </th> 
    <th>Group </th>
    <th>Salutation One </th>
    <th>Salutation Two </th>
    <th>Full Name </th>
    <th>Designation </th>
    <th>Department </th>
    <th>Company </th>
    <th>Staff ID</th>
    <th>Phone </th>    
    <th>Email </th>
    <th>RSVP Confirmation</th>
    <th>Event Attendence</th>

            </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
    <td><?php echo $record->invitees_category;?></td>
    <td><?php echo $record->group_name;?></td>
    <td><?php echo $record->salutation;?></td>
    <td><?php echo $record->salutation_two;?></td>
    <td><?php echo $record->full_name;?></td>
    <td><?php echo $record->designation;?></td>
    <td><?php echo $record->department;?></td>
    <td><?php echo $record->company;?></td>
    <td><?php echo $record->staff;?></td>
    <td><?php echo $record->phone;?></td>
    <td><?php echo $record->email;?></td>

    <td><?php if($record->rsvp_confirmation=='1') {
              echo "Confirmed";
     } else { 
    echo "Not Confirmed";
      } ?> 
    </td>
    <td>Absent</td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
       </div>



   </div>
</div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    } );
} );

</script>