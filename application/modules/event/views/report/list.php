<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Event Report</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Event Report</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchName; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="example">
          <thead>
            <tr>
             <th>Invitation Category </th> 
    <th>Group </th>
    <th>Salutation One </th>
    <th>Salutation Two </th>
    <th>Full Name </th>
    <th>Designation </th>
    <th>Department </th>
    <th>Company </th>
    <th>Staff ID</th>
    <th>Phone </th>    
    <th>Email </th>
    <th>RSVP Confirmation</th>
    <th>Event Attendence</th> 

            </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
                <td><?php echo $record->invitees_category;?></td>
    <td><?php echo $record->group_name;?></td>
    <td><?php echo $record->salutation;?></td>
    <td><?php echo $record->salutation_two;?></td>
    <td><?php echo $record->full_name;?></td>
    <td><?php echo $record->designation;?></td>
    <td><?php echo $record->department;?></td>
    <td><?php echo $record->company;?></td>
    <td><?php echo $record->staff;?></td>
    <td><?php echo $record->phone;?></td>
    <td><?php echo $record->email;?></td>

    <td><?php if($record->rsvp_confirmation=='1') {
              echo "Confirmed";
     } else { 
    echo "Not Confirmed";
      } ?> 
    </td>
    <td>Absent</td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    } );
} );

</script>