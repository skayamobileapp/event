<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3><?php echo $reportname;?></h3>
<a href="/event/report/view" class="btn btn-primary">Back</a>    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category of Invitee</label>
                      <div class="col-sm-8">

               

                         <select type='status' id='invitees' name='invitees' class="form-control">
                            <option value="">Select</option>
                        <?php for($i=0;$i<count($eventCategoryList);$i++) {
                             
                                 ?>
                                <option value="<?php echo $eventCategoryList[$i]->invcat;?>"><?php echo $eventCategoryList[$i]->invcat;?></option>


                        <?php     } ?> 
                    </select>
                      </div>
                    </div>
                  </div>
                   <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Department</label>
                      <div class="col-sm-8">
                        
                         <select type='status' id='department' name='department' class="form-control">
                            <option value="">Select</option>

                        <?php for($i=0;$i<count($eventdepartmentList);$i++) {
                             
                                 ?>
                                <option value="<?php echo $eventdepartmentList[$i]->invcat;?>"><?php echo $eventdepartmentList[$i]->invcat;?></option>


                        <?php     } ?> 
                    </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Company</label>
                      <div class="col-sm-8">
                        
                         <select type='status' id='company' name='company' class="form-control">
                            <option value="">Select</option>

                        <?php for($i=0;$i<count($eventcompanyList);$i++) {
                             
                                 ?>
                                <option value="<?php echo $eventcompanyList[$i]->invcat;?>"><?php echo $eventcompanyList[$i]->invcat;?></option>


                        <?php     } ?> 
                    </select>
                      </div>
                    </div>
                  </div>
                   <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
<select type='status' id='status' name='status' class="form-control">

                          <option value="">All</option>
                          <option value="green">Akan Hadir</option>
                          <option value="red">Tidak Akan Hadir</option>

                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

   <div class="custom-table">
      <table class="table" id="example">
          <thead>
            <tr>
             <th>Invitation Category </th> 
    <th>Group </th>
    <th>Full Name </th>
    <th>Designation </th>
    <th>Department </th>
    <th>Company </th>
    <th>Phone </th>    
    <th>Email </th>
    <th>RSVP Confirmed Date </th>
    <th>Confirmed Via</th>
    <th>Status</th>

            </tr>
          </thead>
           <tbody>
          <?php
          if (!empty($eventprogramme)) {
            $i=1;
            foreach ($eventprogramme as $record) {
          ?>
              <tr>
                <td><?php echo $record->invitees_category;?></td>
    <td><?php echo $record->group_name;?></td>
    <td><?php echo $record->full_name;?></td>
    <td><?php echo $record->designation;?></td>
    <td><?php echo $record->department;?></td>
    <td><?php echo $record->company;?></td>
    <td><?php echo $record->phone;?></td>
    <td><?php echo $record->email;?></td>
    <td><?php 
    if($record->confirmed_date) {
    echo date('d-m-Y',strtotime($record->confirmed_date));
      }?>
          
      </td>

 <td>
    <?php if($record->source=='1') {
              echo "Email";
         } else if($record->source=='2') {
              echo "Whatsapp";
         }  else {
            echo "-";
        }?> 
    </td>
   

    <td><?php if($record->green=='1') {
              echo "Akan Hadir";
         } if($record->red=='1') {
              echo "Tidak Akan Hadir";
         } ?> 
    </td>
   
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
        </table>
    </div>
  </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        pageLength: 100,
        buttons: [
            'excel', 'pdf', 'print'
        ]
    } );
} );

</script>