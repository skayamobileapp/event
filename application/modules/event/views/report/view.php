<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Event Report</h3>
    </div>

    <div class="custom-table">
      <table class="table" id="example" width="100%" border="1">

         <thead>
            <tr>
                <th rowspan="2">Total Invitee</th>
                <th rowspan="2">Total Confirmed</th>
                <th rowspan="2">Total Not Confirmed</th>

                 <th colspan="4" style="text-align: center;">RSVP Attending</th>
                 <th colspan="4" style="text-align: center;">RSVP Not Attending</th>
                 <th colspan="3" style="text-align: center;">Event Attending</th>
            </tr>
             <tr>
                <th>Total</th>
                 <th>WA Confirmed</th>
                 <th>Email Confirmed</th>
                 <th>Not Sure</th>

                  <th>Total</th>
                 <th>WA Confirmed</th>
                 <th>Email Confirmed</th>
                 <th>Not Sure</th>

                  <th>Attended QR</th>
                 <th>Attended Walkin</th>
                 <th>Not Attending</th>

            </tr>
          </thead>

          <?php 

          $totalattending = 0;
          $totalnotattending = 0;
          $attendedqr = 0;
          $attendedwalking = 0;
          $totalattended = 0;
          $confirmdate = 0;
          $waattending = 0;
          $emailattending = 0;
          $notsureattending = 0;
          $wanotattending = 0;
          $emailnotattending = 0;
          $notsurenotattending=0;

          for($i=0;$i<count($invitees);$i++) {

               if($invitees[$i]->confirmed_date!='') {
                $confirmdate++;
              

                    if($invitees[$i]->green=='1') {
                       if($invitees[$i]->source=='2') {
                            $waattending++;
                       }
                       else if($invitees[$i]->source=='1') {
                            $emailattending++;
                       } 
                       else {
                        $notsureattending++;
                       }
                   }
                 
                   if($invitees[$i]->red=='1') {

                       if($invitees[$i]->source=='2') {
                            $wanotattending++;
                       }
                       else if($invitees[$i]->source=='1') {
                            $emailnotattending++;
                       } 
                       else {
                        $notsurenotattending++;
                       }

                    $totalnotattending++;
                   }


                }

          }

          // $totalattending = $confirmdate - $totalnotattending;

          ?>
          <tbody>
              <tr>
                 <td style="text-align: center;"><a href="/event/report/listreport/total"><?php echo count($invitees);?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/totalconfirmed"><?php echo $confirmdate;?></a></td>

                 <td style="text-align: center;"><a href="/event/report/listreport/totalnotconfirmed"><?php echo count($invitees) - $confirmdate;?></a></td>


                 <td style="text-align: center;"><a href="/event/report/listreport/totalattending"><?php echo $waattending + $emailattending + $notsureattending;?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/waattending"><?php echo $waattending;?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/emailattending"><?php echo $emailattending;?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/notsureattending"><?php echo $notsureattending;?></a></td>

                 <td style="text-align: center;"><a href="/event/report/listreport/watotalnotattending"><?php echo $totalnotattending;?></a></td>
                <td style="text-align: center;"><a href="/event/report/listreport/wanotattending"><?php echo $wanotattending;?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/emailnotattending"><?php echo $emailnotattending;?></a></td>
                 <td style="text-align: center;"><a href="/event/report/listreport/notsurenotattending"><?php echo $notsurenotattending;?></a></td>


                 <td></td>
                 <td></td>
                 <td></td>

              </tr>
          </tbody>
   
        </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    } );
} );

</script>