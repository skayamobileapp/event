<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Status extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('status_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['eventStatusList'] = $this->status_model->statussearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Salutation List';
            $this->loadViews("status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('salutation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->status_model->addNewEventStatus($data);
                redirect('/event/status/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Salutation';
            $this->loadViews("status/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('salutation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/event/status/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );

                $result = $this->status_model->editEventStatus($data,$id);
                redirect('/event/status/list');
            }
            $data['eventDetails'] = $this->status_model->getEventStatus($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("status/edit", $this->global, $data, NULL);
        }
    }
}
