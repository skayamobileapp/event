<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Report extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('eventdetails_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation('1');
            $this->global['pageTitle'] = 'Scholarship Management System : Salutation List';
            $this->loadViews("report/list", $this->global, $data, NULL);
        }
    }
}
