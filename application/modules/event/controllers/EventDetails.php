<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EventDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('eventdetails_model');
        $this->load->model('role_model');
        $this->isLoggedIn();
        error_reporting(0);

    }

    function list()
    {
        if ($this->checkAccess('status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['eventDetailsList'] = $this->eventdetails_model->statussearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Salutation List';
            $this->loadViews("eventdetails/list", $this->global, $data, NULL);
        }
    }


    function qrcode(){
        // print_R($_SERVER);exit;
        $path = $_SERVER['DOCUMENT_ROOT'].'/assets/library/phpqrcode/qrlib.php';
          include $path;    
    



    $tempDir = $_SERVER['DOCUMENT_ROOT'].'/assets/qrcode/';
    
    $codeContents = BASE_PATH;
    
    // we need to generate filename somehow, 
    // with md5 or with database ID used to obtains $codeContents...
    $fileName = '005_file_'.md5($codeContents).'.png';
    
    $pngAbsoluteFilePath = $tempDir.$fileName;
   
      
        exit;
    }
    
    function add()
    {
        if ($this->checkAccess('salutation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $limits = $this->security->xss_clean($this->input->post('limits'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $person_name = $this->security->xss_clean($this->input->post('person_name'));
                $person_email = $this->security->xss_clean($this->input->post('person_email'));
                $person_phone = $this->security->xss_clean($this->input->post('person_phone'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
               $link = $this->security->xss_clean($this->input->post('link'));
            
                $invitation_start_date = $this->security->xss_clean($this->input->post('invitation_start_date'));
                $rsvp_deadline = $this->security->xss_clean($this->input->post('rsvp_deadline'));
                $remainder_one = $this->security->xss_clean($this->input->post('remainder_one'));
                $remainder_two = $this->security->xss_clean($this->input->post('remainder_two'));

                $data = array(
                    'title' => $title,
                    'limits' => $limits,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'invitation_start_date' => date('Y-m-d',strtotime($invitation_start_date)),
                    'rsvp_deadline' => date('Y-m-d',strtotime($rsvp_deadline)),
                    'remainder_one' => date('Y-m-d',strtotime($remainder_one)),
                    'remainder_two' => date('Y-m-d',strtotime($remainder_two)),
                    'url' => $url,
                    'address1' => $address1,
                    'remarks' => $remarks,
                    'person_name' => $person_name,
                    'person_email' => $person_email,
                    'person_phone' => $person_phone,
                    'status'=>$status,
                    'link'=>$link
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->eventdetails_model->addNewEvent($data);
                redirect('/event/eventdetails/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Salutation';
            $this->loadViews("eventdetails/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('salutation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/event/status/list');
            }
            if($this->input->post())
            {
                 $title = $this->security->xss_clean($this->input->post('title'));
                $limits = $this->security->xss_clean($this->input->post('limits'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $person_name = $this->security->xss_clean($this->input->post('person_name'));
                $person_email = $this->security->xss_clean($this->input->post('person_email'));
                $person_phone = $this->security->xss_clean($this->input->post('person_phone'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $link = $this->security->xss_clean($this->input->post('link'));
            
                $invitation_start_date = $this->security->xss_clean($this->input->post('invitation_start_date'));
                $rsvp_deadline = $this->security->xss_clean($this->input->post('rsvp_deadline'));
                $remainder_one = $this->security->xss_clean($this->input->post('remainder_one'));
                $remainder_two = $this->security->xss_clean($this->input->post('remainder_two'));

                $data = array(
                    'title' => $title,
                    'limits' => $limits,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'invitation_start_date' => date('Y-m-d',strtotime($invitation_start_date)),
                    'rsvp_deadline' => date('Y-m-d',strtotime($rsvp_deadline)),
                    'remainder_one' => date('Y-m-d',strtotime($remainder_one)),
                    'remainder_two' => date('Y-m-d',strtotime($remainder_two)),
                    'url' => $url,
                    'address1' => $address1,
                    'remarks' => $remarks,
                    'person_name' => $person_name,
                    'person_email' => $person_email,
                    'person_phone' => $person_phone,
                    'status'=>$status,
                    'link'=>$link
                );

                $result = $this->eventdetails_model->editEventDetails($data,$id);
                redirect('/event/eventDetails/list');
            }

                        $data['eventStatusList'] = $this->eventdetails_model->statuseventsearchList();

            $data['eventdetails'] = $this->eventdetails_model->getEventDetails($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/edit", $this->global, $data, NULL);
        }
    }


    public function programme($id)
    {
        


        if ($_POST) {



            $activity = array(
            'remarks'=>$_POST['remarks'],
            'programme'=>$_POST['programme'],
            'timings'=>$_POST['timings'],
            'id_event_days'=>$_POST['id_event_days'],
            'id_event_title'=>$id
        );


            if($_FILES) {

                $file = $_FILES['document']['name'];
                $file_tmp = $_FILES['document']['tmp_name'];
                $key = "Programme File";
                 $activity['document'] = $this->uploadFile($file,$file_tmp,$key);
            }
                

        $this->eventdetails_model->addEventProgramme($activity);   


        }

        $data['eventprogramme']  = $this->eventdetails_model->getEventProgramme($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/programme", $this->global, $data, NULL);


    }


    public function communication($id) {
                $eventDetails  = $this->eventdetails_model->getEventDetails($id);

          if ($_POST) {

                if($_POST['id_invitation']) {
                  $inviteeDetails  = $this->eventdetails_model->getInviteeDetails($_POST['id_invitation']);
                  }


              if($_POST['templateid']) {

                $activity = array(
                    'email_subject'=>$_POST['subject'],
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->updateTemplate($activity,$_POST['templateid']); 

            

              } else {
                $activity = array(
                    'email_subject'=>$_POST['subject'],
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->addTemplate($activity); 
             }

             /// Replaing the strings

             $messageReplacedString = $this->replacemessagestrings($_POST['message'],$inviteeDetails,$eventDetails);
             $waReplacedString = $this->replacemessagestrings($_POST['message_wa'],$inviteeDetails,$eventDetails);




            if($_POST['send_as']=='Email_Whatsapp' || $_POST['send_as']=='Whatsapp') {
                    $apiURL = 'https://api.chat-api.com/instance243272/';
                    $token = 'x9bztflvg1ty5fnf';
                    $phone = $inviteeDetails->phone;

                     $message = strip_tags($waReplacedString);

                     $message = str_replace("&nbsp;"," ",$message);
                     $message = str_replace("&ndash;","-",$message);
                    // $message = html_entity_decode($waReplacedString);


                    $data = json_encode(array(
                        'chatId'=>$phone.'@c.us',
                        'body'=>$message
                    ));
                    $url = $apiURL.'sendMessage?token='.$token;
                    $options = stream_context_create(['http' => [
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/json',
                        'content' => $data
                    ]
                    ]);
                     $response = file_get_contents($url,false,$options);
            } 
            if($_POST['send_as']=='Email_Whatsapp' || $_POST['send_as']=='Email') {
                    $subject = $_POST['subject'];
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    // More headers
                    $headers .= 'From: <admin@agilidigitaltechnologies.com>' . "\r\n";
                    $headers .= 'Cc: askiran123@gmail.com' . "\r\n";
                    $mail = mail($inviteeDetails->email,$subject,$messageReplacedString,$headers);

            } 


                redirect('/event/eventDetails/communication/'.$id);

          }


        $data['eventtemplate']  = $this->eventdetails_model->getTemplate($id);
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/email_invitation", $this->global, $data, NULL);
    }


    public function replacemessagestrings($message,$invitee,$programme) {

        //Name
        // print_R($message);exit;
// print_r($invitee);
        $rsvpencoded = $invitee->uniqueid;

        $rsvplink = BASE_PATH."registration/index/$rsvpencoded";


        $message = str_replace("@salutation",$invitee->salutation,$message);
        $message = str_replace("@studentname",$invitee->full_name,$message);
        $message = str_replace("@email",$invitee->email,$message);
        $message = str_replace("@department",$invitee->department,$message);
        $message = str_replace("@staffid",$invitee->staff_id,$message);
        $message = str_replace("@eventname",$programme->title,$message);
        $message = str_replace("@department",$invitee->department,$message);
        $message = str_replace("@designation",$invitee->designation,$message);
        $message = str_replace("@office",$invitee->office,$message);
        $message = str_replace("@sector",$invitee->sector,$message);
        $message = str_replace("@management",$invitee->management,$message);
        $message = str_replace("@part",$invitee->part,$message);
        $message = str_replace("@unit",$invitee->unit,$message);
        $message = str_replace("@rsvplink",$rsvplink,$message);
        $message = str_replace("@company",$invitee->company,$message);
        $message = str_replace("@inviteescategory",$invitee->invitees_category,$message);
        $message = str_replace("@salutation2",$invitee->salutation_two,$message);
        $message = str_replace("@rsvpdeadline",$programme->rsvp_deadline,$message);
        $message = str_replace("@rsvplink",$rsvplink,$message);

        return $message;
    }


     public function setting($id) {
         if ($_POST) {

              if($_POST['templateid']) {

                $activity = array(
                    'template'=>$_POST['message'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->updateSettingTemplate($activity,$_POST['templateid']); 


              } else {
                $activity = array(
                    'template'=>$_POST['message'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->addSettingTemplate($activity); 
             }  

          }


        $data['eventtemplate']  = $this->eventdetails_model->getSettingTemplate($id);
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/rsvp_setting", $this->global, $data, NULL);
    }


     public function confirmationsetting($id) {
         if ($_POST) {

               if($_POST['templateid']) {

                $activity = array(
                    'email_subject'=>$_POST['subject'],
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->updateConfirmationTemplate($activity,$_POST['templateid']); 

            

              } else {
                $activity = array(
                    'email_subject'=>$_POST['subject'],
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->addConfirmationTemplate($activity); 
             }

          }


        $data['eventtemplate']  = $this->eventdetails_model->getConfirmationTemplate($id);
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/confirmationemail", $this->global, $data, NULL);
    }


     public function failuresetting($id) {
         if ($_POST) {

               if($_POST['templateid']) {

                $activity = array(
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->updateFailureTemplate($activity,$_POST['templateid']); 

            

              } else {
                $activity = array(
                    'email_template'=>$_POST['message'],
                    'whatsap_template'=>$_POST['message_wa'],
                    'id_event_title'=>$id
                );
             $this->eventdetails_model->addFailureTemplate($activity); 
             }

          }


        $data['eventtemplate']  = $this->eventdetails_model->getFailureTemplate($id);
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/failureemail", $this->global, $data, NULL);
    }




      public function rsvp($id) {
        $data['eventprogramme']  = $this->eventdetails_model->getRSVPInvitation($id);


            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/rsvp", $this->global, $data, NULL);
    }


   public function seat($id) {
        $data['eventprogramme']  = $this->eventdetails_model->getRSVPInvitation($id);

        $data['eventdays']  = $this->eventdetails_model->getEventDays($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/seat", $this->global, $data, NULL);
    }



 public function attendence($id) {
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

        $data['eventdays']  = $this->eventdetails_model->getEventDays($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/attendence", $this->global, $data, NULL);
    }




    public function invitation($id)
    {
        //title

        if($_FILES){


            if (move_uploaded_file($_FILES['activityfile']['tmp_name'], DOCUMENT_PATH. $_FILES["activityfile"]['name'])) {
            } 
            $onload = 1;

            chmod(DOCUMENT_PATH.$_FILES["activityfile"]['name'], 0777); 
            $file = fopen(DOCUMENT_PATH.$_FILES["activityfile"]['name'], "r");

            $i=0;

                 while(! feof($file))
                    {
                        if($i==0) {
                            $array = fgetcsv($file);

                        } else {
               
                            $userDetails = array();
                             //check for type 
                            $array = fgetcsv($file);
                            if($array[0]=='') {
                                continue;
                            }
                            $userDetails = $this->eventdetails_model->getInvitationByNRIC($id,$array[0]);
                            if($userDetails) {
                                $data = array(
                                'salutation'      => $array[0],
                                'first_name'=> $array[1],
                                'full_name' =>$array[3],
                                'last_name' => $array[2],
                                'nric'    => $array[4],
                                'email'     => $array[5],
                                'phone'     => $array[6],
                                'gender' => $array[7],
                                'organisation' => $array[8],
                                'department' => $array[9],
                                'designation' => $array[10],
                                'staff_id' => $array[11],
                                'office' => $array[12],
                                'sector' => $array[13],
                                'management' => $array[14],
                                'part' => $array[15],
                                'unit' => $array[16],
                                'sub_unit' => $array[17],
                                'location' => $array[18],
                                'id_event_category' => $array[19],
                                'id_event_title' =>$id
                                );
                            $this->eventdetails_model->updateInvitation($array[0],$id,$data);



                            } else {
                                $data = array(
                                'salutation'      => $array[0],
                                'first_name'=> $array[1],
                                'full_name' =>$array[3],
                                'last_name' => $array[2],
                                'nric'    => $array[4],
                                'email'     => $array[5],
                                'phone'     => $array[6],
                                'gender' => $array[7],
                                'organisation' => $array[8],
                                'department' => $array[9],
                                'designation' => $array[10],
                                'staff_id' => $array[11],
                                'office' => $array[12],
                                'sector' => $array[13],
                                'management' => $array[14],
                                'part' => $array[15],
                                'unit' => $array[16],
                                'sub_unit' => $array[17],
                                'location' => $array[18],
                                'id_event_category' => $array[19],
                                'id_event_title' =>$id
                                );
                                $cpd_id = $this->eventdetails_model->addEventInvitation($data);
                            }
                        }
                        $i++;
                    
            } 
        }
        $data['eventprogramme']  = $this->eventdetails_model->getInvitation($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("eventdetails/invitation", $this->global, $data, NULL);

    }






 
}
