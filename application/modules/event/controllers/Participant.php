<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Participant extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('participant_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['eventParticipantList'] = $this->participant_model->statussearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Salutation List';
            $this->loadViews("participant/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('salutation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->participant_model->addNewEventParticipant($data);
                redirect('/event/participant/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Salutation';
            $this->loadViews("participant/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('salutation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/event/status/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status
                );

                $result = $this->participant_model->editEventParticipant($data,$id);
                redirect('/event/participant/list');
            }
            $data['categoryDetails'] = $this->participant_model->getEventParticipant($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Salutation';
            $this->loadViews("participant/edit", $this->global, $data, NULL);
        }
    }
}
