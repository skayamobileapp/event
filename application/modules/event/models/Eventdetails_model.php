<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Eventdetails_model extends CI_Model
{
    function statussearchList()
    {
        $this->db->select('*');
        $this->db->from('event_details');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function statussearch($search)
    {
        $this->db->select('*');
        $this->db->from('event_details');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or sequence  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getEventDays($id){
        $this->db->select('*');
        $this->db->from('event_days');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    function getEventDetails($id)
    {
        $this->db->select('*');
        $this->db->from('event_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

     function statuseventsearchList()
    {
        $this->db->select('*');
        $this->db->from('event_status');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getInvitation($id){
         $this->db->select('*');
        $this->db->from('event_invitation');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getRSVPInvitation($id){
         $this->db->select('*');
        $this->db->from('event_invitation');
        $this->db->where('id_event_title', $id);
        $this->db->where("status='1'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getInviteeDetails($id) {
         $this->db->select('*');
        $this->db->from('event_invitation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getSettingTemplate($id){
         $this->db->select('*');
        $this->db->from('event_setting_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTemplate($id){
         $this->db->select('*');
        $this->db->from('event_mail_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }




    function getConfirmationTemplate($id){
         $this->db->select('*');
        $this->db->from('event_confirmation_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function getFailureTemplate($id){
         $this->db->select('*');
        $this->db->from('event_failure_template');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getEventProgramme($id)
    {

         $this->db->select('*');
        $this->db->from('event_programme');
        $this->db->where('id_event_title', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewEvent($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addTemplate($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_mail_template', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

     function addConfirmationTemplate($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_confirmation_template', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

        function addFailureTemplate($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_failure_template', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addSettingTemplate($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_setting_template', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addEventInvitation($data){
                $this->db->trans_start();
        $this->db->insert('event_invitation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function getInvitationByNRIC($id,$nric){
        $this->db->select('*');
        $this->db->from('event_invitation');
        $this->db->where('id_event_title', $id);
        $this->db->where('nric', $nric);
        $query = $this->db->get();
        return $query->row();
    }


    function addEventProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function updateInvitation($nric,$id,$data){
        $this->db->where('nric', $nric);
        $this->db->where('id_event_title', $id);
        $this->db->update('event_invitation', $data);
        return TRUE;

    }


    function updateTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_mail_template', $data);
        return TRUE;
    }

      function updateConfirmationTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_confirmation_template', $data);
        return TRUE;
    }


     function updateFailureTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_failure_template', $data);
        return TRUE;
    }


     function updateSettingTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_setting_template', $data);
        return TRUE;
    }

    function editEventDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_details', $data);
        return TRUE;
    }
}

