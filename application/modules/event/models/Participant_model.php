<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Participant_model extends CI_Model
{
    function statussearchList()
    {
        $this->db->select('*');
        $this->db->from('event_participant');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function statussearch($search)
    {
        $this->db->select('*');
        $this->db->from('event_participant');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or sequence  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getEventParticipant($id)
    {
        $this->db->select('*');
        $this->db->from('event_participant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewEventParticipant($data)
    {
        $this->db->trans_start();
        $this->db->insert('event_participant', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEventParticipant($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event_participant', $data);
        return TRUE;
    }
}

