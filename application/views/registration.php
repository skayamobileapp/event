<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <title>Home</title>
  
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;600&display=swap"
      rel="stylesheet"
    />
    <style type="text/css">
      html {
  font-family: sans-serif;
  line-height: 1.15;
}
body {
  background-color: #f5f4f8;
  margin: 0;
  font-family: 'Oswald', sans-serif;
  font-size: 1rem;
  font-weight: 600;
  line-height: 1.5;
  color: #fff;
  text-align: left;
}
*,
::after,
::before {
  box-sizing: border-box;
}

.event-container {
  background: url('../../assets/img/event_bg.jpg') no-repeat center top #1d0030;
  max-width: 800px;
  height: 1130px;
  background-size: 100%;
  margin: 0 auto;
  position: relative;
}

.top-block {
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  text-align: center;
  top: 120px;
}
.top-block h4 {
  color: #ffdfa9;
  text-transform: uppercase;
  font-size: 1.125rem;
  margin-bottom: 0px;
  letter-spacing: 3px;
}
.top-block h3 {
  text-transform: uppercase;
  font-size: 1.25rem;
  letter-spacing: 3px;
}
.body-copy {
  font-size: 1.125rem;
  position: absolute;
  top: 610px;
  max-width: 500px;
  width: 500px;
  left: 50%;
  transform: translatex(-50%);
  text-align: center;
}
.grey-block {
  background-color: #6d6e70;
  max-width: 600px;
  margin: 0 auto;
  text-align: center;
  position: absolute;
  width: 600px;
  left: 50%;
  transform: translateX(-50%);
  top: 740px;
}

.button-block .btn {
  padding: 5px;
  border: 2px solid #fff;
  background-color: #71e99f;
  color: #000;
  font-family: 'Oswald', sans-serif;
  font-size: 1.25rem;
  box-shadow: 0px 0px 0px 3px #71e99f;
  min-width: 150px;
  cursor: pointer;
}
.button-block .btn:hover {
  background-color: #4cb173;
}
.button-block .btn + .btn {
  margin-left: 30px;
}

.button-block .btn-red {
  background-color: #be1e2d;
  color: #fff;
  box-shadow: 0px 0px 0px 3px #be1e2d;
}
.button-block .btn-red:hover {
  background-color: #94121f;
}
.grey-block .italic {
  font-style: italic;
}


    </style>
  </head>
  <body>
  <form method="POST" action="">
    <div class="event-container">
      <div class="top-block">
        <h4>Menjemput</h4>
        <h3><?php echo $inviteeDetails->salutation_two;?> <?php echo $inviteeDetails->full_name;?></h3>
      </div>
      <p class="body-copy">

        <?php
        $catarray = explode('*',$inviteeDetails->invitees_category);
        for($i=0;$i<count($catarray);$i++) {
          echo $catarray[$i].'<br/>';
        }
        ?>
      </p>
      <div class="grey-block">
        <p>Saya mengesahkan<br />(Sila KLIK petak yang bersesuaian di bawah)</p>
        <input type='hidden' name='evename' id='evename' value='1'/>
        <div class="button-block">
          <button type='submit' class="btn btn-green">Akan hadir</button>
          <button type='submit' class="btn btn-red">Tidak akan hadir</button>
        </div>
        <p class="italic">* *RSVP Sebelum 6 April 2021 (Selasa)</p>
      </div>
    </div>
      </form>

    <!-- Optional JavaScript; choose one of the two! -->
    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>

<script type="text/javascript">
  function thankyou(){

  }
</script>