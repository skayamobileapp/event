<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CIDB - Opuvent</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <style>
    /* LOGIN CSS STARTS HERE */

.login-new-container {
  background-color: #fff;
  min-height: 100vh;
  margin-left: -15px;
  padding: 1rem 3rem;
}
.login-new-container h1 {
  color: #000;
  text-transform: uppercase;
  font-family: "rubikbold";
}
.login-new-container h3 {
  color: #000;
  margin-top: 3rem;
  margin-bottom: 3rem;
}
.login-new-container .btn-primary {
  margin-bottom: 2rem;
  height: 40px;
  margin-top: 2rem;
}
.d-flex {
  display: flex;
}
.align-items-center {
  align-items: center;
}
.ml-auto {
  margin-left: auto;
}

</style>
</head>

<body>
  <form method="POST" action="">
    <div class="login-wrapper-rsvp">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="login-new-container d-flex align-items-center">            
                <div class="event-wrapper">
                <div class="text-center">
                  <a href="/"><img class="logo" src="<?php echo BASE_PATH; ?>assets/img/cidb_logo.png" /></a>     
                </div>                  
                  <h3 class="text-center">MAJLIS PENGHARGAAN STAF 2021</h3>


                   <div class="clearfix">
                     <div class="row">
                      <div class="col-sm-12">
                                                 <p style="font-size:14px;text-align: center;">Terima kasih</p>

                         <p style="font-size:14px;text-align: center;">Anda telah mengesahkan kehadiran. Sila hubungi pihak Sekretariat Majlis Penghargaan Staff 2021 CIDB jika terdapat sebarang perubahan</p>                      </div>
                     </div>

                   </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> 
  </form> 
  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>

</html>