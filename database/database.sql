-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2021 at 10:18 AM
-- Server version: 5.7.33-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-37+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1),
(4, 'Salutation', 'Setup', 'General Setup', NULL, 'salutation', 'list', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1),
(14, 'Permision View', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14),
(548, 14, 10),
(549, 14, 11),
(550, 14, 12),
(551, 14, 13),
(552, 14, 14),
(553, 14, 15),
(554, 14, 7),
(555, 14, 8),
(556, 14, 9);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'AG', 0, 1, NULL, '2020-08-10 03:08:19', NULL, '2020-08-10 03:08:19'),
(2, 'DATIN', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(3, 'DATINDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(4, 'DATINPROFDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(5, 'DATINSERI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(6, 'DATINSETIA', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(7, 'DATINSRI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(8, 'DATO', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(9, 'DATODR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(10, 'DATOHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(11, 'DATOSRI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(12, 'DATUK', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(13, 'DATUKHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(14, 'DATUKSERI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(15, 'DATUKSERIDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(16, 'DR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(17, 'DRHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(18, 'EN', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(19, 'HAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(20, 'HE', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(21, 'JENBTANSRIDATO', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(22, 'MDM', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(23, 'MR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(24, 'MRS', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(25, 'MS', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_last_login`
--

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 1, '{"role":"1","roleText":"Administrator","name":"Administrator"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'admin@gmail.com', '$2y$10$YykrXgnhZ563BCiC6L2ee.Kktd/Vj/KncTnTeDZQXCWwAQ1xH4Req', 'Administrator', '9890098901', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49'),
(10, 'permissions@cms.com', '$2y$10$IGMpNysy12MZbrSP50tYsuJCAIQH/N8f3VaqsafRP9Ip58W9pMU7.', 'Permission Assigner', '479123491', 14, 0, 1, '2020-10-31 11:34:16', 1, '2020-10-31 11:52:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
